FROM php:8.1.5-apache

RUN apt-get update && apt-get install -y git curl zip unzip libzip-dev \
    libtiff-tools imagemagick libmariadb-dev
RUN docker-php-ext-install mysqli pdo_mysql
RUN a2enmod rewrite

ENV APACHE_LOG_DIR=/var/log/apache2

COPY src /var/www/html/
COPY php.ini /usr/local/etc/php/


RUN chown -R www-data:www-data /var/www
